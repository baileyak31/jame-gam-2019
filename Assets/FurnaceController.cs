﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnaceController : EnemyController
{
    public GameObject player;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        health = 5;
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("health", health);
        if (health <= 0)
            Destroy(gameObject);
    }

}
