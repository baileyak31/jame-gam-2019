﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    private bool facingRight = true;
    public float aerialAcceleration = 1.0f;
    public int health = 3;
    public LayerMask groundLayer;
    public Rigidbody2D myRigidbody2D;
    public AudioSource audioSource;
    private Vector2 moveDirection = Vector2.zero;
    public Animator animator;

    [FMODUnity.EventRef]
	public string footsteps = "event:/player_footsteps";			//declare the sound name and event path
    FMOD.Studio.EventInstance footstepsEv;                //rolling event
    FMOD.Studio.ParameterInstance footstepsParam;    //Declare the speed param object

    // Start is called before the first frame update
    void Start()
    {
        footstepsEv = FMODUnity.RuntimeManager.CreateInstance(footsteps);  
        footstepsEv.getParameter("", out footstepsParam);
        footstepsEv.start();
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
            Destroy(gameObject);
        // Move the controller
        animator.SetBool("walking", Input.GetAxis("Horizontal") != 0 && IsGrounded());
        animator.SetBool("grounded", IsGrounded());
        animator.SetBool("attacking", Input.GetButtonDown("Fire1"));
        if ((Input.GetAxis("Horizontal") > 0.0 && !facingRight) || (Input.GetAxis("Horizontal") < 0.0 && facingRight))
            FlipCharacter();

        if (IsGrounded() && Input.GetButton("Jump"))
        {
            audioSource.Play();
            myRigidbody2D.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, jumpSpeed);
            //myRigidbody2D.AddForce(Vector2.up * jumpSpeed);
        }

        FMOD.Studio.PLAYBACK_STATE play_state;
        footstepsEv.getPlaybackState(out play_state);
        if (Input.GetAxis("Horizontal") != 0.0 && IsGrounded() && play_state != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            footstepsEv.start();
        }
        else if ((Input.GetAxis("Horizontal") == 0.0 || !IsGrounded()) && play_state == FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            footstepsEv.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
        myRigidbody2D.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, myRigidbody2D.velocity.y);
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (Input.GetButtonDown("Fire1") && collider.tag == "Enemy")
        {
            collider.gameObject.GetComponent<EnemyController>().TakeDamage(1);
        }
    }

    bool IsGrounded() {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 2.0f;
 
        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, groundLayer);
        if (hit)
            return true;
 
        return false;
    }

    void FlipCharacter()
    {
        facingRight = !facingRight;
        var localScaleCopy = transform.localScale;
        localScaleCopy.x *= -1;
        transform.localScale = localScaleCopy;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
            --health;
    }
}
