﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunnyController : EnemyController 
{
    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    private bool isGrounded = false;
    public float moveDirection = -1.0f;
    public LayerMask groundLayer;

    // Start is called before the first frame update
    void Start()
    {
        health = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
            Destroy(gameObject);

        Vector2 position = transform.position;
        float distance = 1.5f;
 
        RaycastHit2D hit = Physics2D.Raycast(position, new Vector2(moveDirection, 0.0f), distance, groundLayer);
        if (hit)
            FlipCharacter();
 
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveDirection * speed, 0.0f);
    }

    void FlipCharacter()
    {
        moveDirection *= -1;
        var localScaleCopy = transform.localScale;
        localScaleCopy.x *= -1;
        transform.localScale = localScaleCopy;
    }

}
